import csv
import json
import sys

with open(str(sys.argv[1]), "r") as f:  # open your CSV file for reading
    next(f)
    # DictReader for convenience
    reader = csv.DictReader(f, fieldnames=['id', 'path', 'operation', 'INSERT', 'SecurityContext',
                                           'SecurityContextValue', 'RoleNameCr', 'UserName'])
    data = [{"id": r.pop("id", None), "path": r.pop("path", None), "operation": r.pop("operation", None), "payload": r}
            for r in reader]  # convert!
    # finally, serialize the data to JSON
    data_json = json.dumps(data, indent=4)
    # print(data_json)
    json_file = open("data.json", "w")
    json_file.write(data_json)
